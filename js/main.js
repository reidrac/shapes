var Game = function(id) {
	var self = {
		id: id,
		canvas: undefined,
		ctx: undefined,

		touch_current: undefined,
		touch_on: false,

		resources: {},

		dt: 0,
		then: 0,

		cmax: 200,
		timer: 0,
		cool_down: 30,
		effects: [],

		loading: 0,
		total_loading: 4,

		selected: "circle" 
	};

	self.change = function(type) {
		var e = document.getElementById(self.selected);
		e.style.border = "2px solid #000";
		var e = document.getElementById(type);
		e.style.border = "2px solid #fff";
		self.selected = type;
	};

	self.draw = function() {
		self.ctx.save();
		self.ctx.fillStyle = "rgb(0, 0, 0)";
		self.ctx.textAlign = "center";
		self.ctx.font = "bold 8pt Arial";
		self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);

		if (self.loading != self.total_loading)
		{
			self.ctx.fillStyle = "rgb(255, 255, 255)";
			self.ctx.fillText("Loading " + (self.loading + 1) + " of " + self.loading_total, Math.floor(self.canvas.width/2), Math.floor(self.canvas.height/3));
			self.ctx.restore();
			return;
		}

		self.effects = self.effects.filter(function(e) {
			return e.c < self.cmax;
		}).map(function(e) {
			var a = 1 - (e.c / self.cmax);

			switch(e.type) {
				case "circle":
					self.ctx.beginPath();
					self.ctx.arc(e.x, e.y, e.c, 2 * Math.PI, false);		
					self.ctx.fillStyle = "rgba(" + e.r + "," + e.g + "," + e.b + "," + a + ")";
					self.ctx.fill();
					e.c += 2;
					break;
				case "square":
					self.ctx.save();
					self.ctx.beginPath();
					self.ctx.strokeStyle = "rgba(" + e.r + "," + e.g + "," + e.b + "," + a + ")";
					self.ctx.translate(e.x, e.y);		
					self.ctx.rotate(e.c * Math.PI / 180);
					self.ctx.lineWidth = 5;
					self.ctx.strokeRect(-Math.floor(e.c / 2), -Math.floor(e.c / 2), e.c, e.c);		
					e.c += 2;
					self.ctx.restore();
					break;
				case "star":
					var rot = Math.PI / 2 * 3;
					var x = e.x;
					var y = e.y;
					var step = Math.PI / 5;

					self.ctx.fillStyle = "rgba(" + e.r + "," + e.g + "," + e.b + "," + a + ")";

					self.ctx.beginPath();
					self.ctx.moveTo(x, y - e.c);

					var i;
					for (i = 0; i < 5; i++) {
						x = e.x + Math.cos(rot) * e.c;
						y = e.y + Math.sin(rot) * e.c;
						self.ctx.lineTo(x, y);
						rot += step;
						x = e.x + Math.cos(rot) * e.c * 0.4;
						y = e.y + Math.sin(rot) * e.c * 0.4;
						self.ctx.lineTo(x, y);
						rot += step;
					}

					self.ctx.lineTo(e.x, e.y - e.c);
					self.ctx.closePath();
					self.ctx.fill();

					e.c += 2;
					self.ctx.restore();
					break;

				case "balls":
					self.ctx.beginPath();
					e.r = 255 - Math.floor(Math.sin(e.c * Math.PI/180) * 128);
					e.g = 255 - Math.floor(Math.cos(e.c * Math.PI/180) * 128);
					e.b = 255 - Math.floor((e.r + e.g) / 2);
					self.ctx.arc(e.x, e.y, 20, 2 * Math.PI, false);		
					if (e.c < self.cmax / 2) {
						self.ctx.fillStyle = "rgb(" + e.r + "," + e.g + "," + e.b + ")";
					} else {
						self.ctx.fillStyle = "rgba(" + e.r + "," + e.g + "," + e.b + "," + a * 2 + ")";
					}
					self.ctx.fill();
					e.c++;
					break;
			}
			return e;
		});

		self.ctx.restore();
	};

	self.update = function update(dt) {
		if (self.touch_on && self.effects.length < 10) {
			if (self.timer) {
				self.timer--;
			} else {
				self.timer = self.cool_down;

				var x = self.touch_current.x;
				var y = self.touch_current.y;

				self.effects.push({ 
					type: self.selected,
					x: x, 
					y: y,
					c: 0,
					r: 255 - Math.floor(Math.random() * 64),
					g: 255 - Math.floor(Math.random() * 64),
					b: 255 - Math.floor(Math.random() * 64)
				})
				createjs.Sound.play(self.selected);
			}
		} else {
			self.timer = 0;
		}
	};
	
	self.run = function() {
		self.loop(0);
	}

	self.loop = function loop(now) {
		var k = 1 / 80;
		self.dt += Math.min(1 / 60, now - self.then) || 0;
		while(self.dt >= k) {
			self.update(k);
			self.dt -= k;
		}

		self.draw();

		self.then = now;
		requestAnimationFrame(self.loop);
	};

	self.touch_find_current = function(list) {
		var i;
		for(i=0; i<list.length; i++) {
			if(list[i].identifier == self.touch_current.id) {
				return list[i];
			}
		}
		return undefined;
	};

	self.touch_start = function(event) {
		self.touch_on = true;
		var e = event.changedTouches[0];
		self.touch_current = { id: e.identifier, x: parseInt(e.clientX) - e.target.offsetLeft, y: parseInt(e.clientY) - e.target.offsetTop };
		event.preventDefault();
	};

	self.touch_move = function(event) {
		var e = self.touch_find_current(event.changedTouches);
		if (e != undefined) {
			self.touch_current = { id: e.identifier, x: parseInt(e.clientX) - e.target.offsetLeft, y: parseInt(e.clientY) - e.target.offsetTop };
		}
		event.preventDefault();
	};

	self.touch_end = function(event) {
		if(self.touch_on) {
			var c = self.touch_find_current(event.changedTouches);
			if(c != undefined) {
				self.touch_on = false;
				self.touch_current = undefined;
			}
		}
		event.preventDefault();
	};

	self.on_resize = function() {
		var e = document.getElementById("buttons");
		self.canvas.width = window.innerWidth;
		self.canvas.height = window.innerHeight - e.offsetHeight;
	};

	self.canvas = document.getElementById(self.id);
	if (!self.canvas.getContext) {
		self.canvas.insertAdjacentHTML("afterend", "<h1>This game requires canvas 2D support :(</h1>");
		return undefined;
	}

	self.canvas.style.background = "rgb(21, 21, 21)";
	self.canvas.addEventListener('touchstart', self.touch_start, false);
	self.canvas.addEventListener('touchmove', self.touch_move, false);
	self.canvas.addEventListener('touchend', self.touch_end, false);
	self.ctx = self.canvas.getContext("2d");

	window.addEventListener('resize', self.on_resize, false);

	self.loaded = function() {
		self.loading++;
	}

	var audio = {
		circle: "snd/circle.ogg",
		square: "snd/square.ogg",
		star: "snd/star.ogg",
		balls: "snd/balls.ogg"
	};

	createjs.Sound.addEventListener("fileload", createjs.proxy(self.loaded));
	for(index in audio) {
		createjs.Sound.registerSound(audio[index], index);
	}

	return self;
};

window.onload = function () {
	document.body.addEventListener('touchmove', function(event) {
		event.preventDefault();
	}, false);
	game = Game("game");
	game.change("circle");
	game.on_resize();
	if (game != undefined) {
		game.loop(0);
	}
};
